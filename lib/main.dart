import 'package:flutter/material.dart';
import 'package:tourism_place/main_screen.dart';

void main() => runApp(Myapp());

class Myapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Travel",
      theme: ThemeData.dark(),
      home: MainScreen(),
    );
  }
}

//kita bukak mainscreen, DetailScreen sudah diarahkan ke masing-masing tempat
